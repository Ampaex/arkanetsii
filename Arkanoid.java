package pgpi_arkanoid;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;

public class Arkanoid extends JFrame implements KeyListener {

	/* GAME VARIABLES */

	private boolean tryAgain = false;   //Variable para controlar si se permite seguir intentándolo
	private boolean running = false;    //Varibale que indice si se está ejecutando el juego

	private Paddle paddle = new Paddle(Parametros.SCREEN_WIDTH / 2, Parametros.SCREEN_HEIGHT - 50); //crea paleta
	private Ball ball = new Ball(Parametros.SCREEN_WIDTH / 2, Parametros.SCREEN_HEIGHT / 2); //crea bola
	private List<Brick> bricks = new ArrayList<Brick>(); //crea lista de ladrillos
	private ScoreBoard scoreboard = new ScoreBoard();    //crea tablero de puntuación

	private double lastFt;          //último fotograma, guarda el tiempo en que se ejecutó 
	private double currentSlice;    //acumulador de tiempo de fotogramas

    //Método para detectar intersección entre dos objetos, devuelve true si la hay
	boolean isIntersecting(GameObject mA, GameObject mB) {
        /* Los objetos tienen unas coordenadas que determinan su centro. Las funciones left, right, top y 
           bottom determinan el área ocupada por dichos objetos. Comparando estos 4 valores de los dos objetos
           podemos ver si se está produciendo una intersección */
		return mA.right() >= mB.left() && mA.left() <= mB.right() 
				&& mA.bottom() >= mB.top() && mA.top() <= mB.bottom();
	}

    //Metodo para detectar colisiones del paddle con la bola
	void testCollision(Paddle mPaddle, Ball mBall) {
		if (!isIntersecting(mPaddle, mBall)) //si hay intersección sale
			return;
		mBall.velocityY = -Parametros.BALL_VELOCITY;    //cambio el sentido de la bola en el eje Y
		if (mBall.x < mPaddle.x)                        //si está la boca a la izquierda de la paleta
			mBall.velocityX = -Parametros.BALL_VELOCITY;    //cambia el sentido de la bola en el eje X
		else
			mBall.velocityX = Parametros.BALL_VELOCITY; //no cambia en sentido en el eje X
	}

    //Método para detectar colisiones de la bola con los ladrillos
	void testCollision(Brick mBrick, Ball mBall, ScoreBoard scoreboard) {
		if (!isIntersecting(mBrick, mBall)) //si no hay colisión sale
			return;

		mBrick.destroyed = true; //se destruye el ladrillo

		scoreboard.increaseScore(); //incrementa la puntuación

        //calcula superposiciones según los tamaños de los objetos y sus posiciones:
		double overlapLeft = mBall.right() - mBrick.left();     //bola a la izquierda, ladrillo a la derecha
		double overlapRight = mBrick.right() - mBall.left();    //ladrillo a la izquierda, bola a la derecha
		double overlapTop = mBall.bottom() - mBrick.top();      //bola arriba, ladrillo debajo
		double overlapBottom = mBrick.bottom() - mBall.top();   //ladrillo arriba, bola debajo

		boolean ballFromLeft = overlapLeft < overlapRight; //calcula si la bola viene de la izquierda
		boolean ballFromTop = overlapTop < overlapBottom;  //calcula si la bola viene de arriba

        /* con los valores anteriores calcula valores de superposición de X,Y según venga de la izquierda o derecha
           y de abajo o de arriba */ 
		double minOverlapX = ballFromLeft ? overlapLeft : overlapRight; 
		double minOverlapY = ballFromTop ? overlapTop : overlapBottom;

        /* recalcula las velocidades en ambos ejes en un sentido u otro dependiendo de si choca a la izquierda, derecha
            arriba o abajo */
		if (minOverlapX < minOverlapY) {
			mBall.velocityX = ballFromLeft ? -Parametros.BALL_VELOCITY : Parametros.BALL_VELOCITY;
		} else {
			mBall.velocityY = ballFromTop ? -Parametros.BALL_VELOCITY : Parametros.BALL_VELOCITY;
		}
	}
    
    //Método de inicilización de los ladrillos
	void initializeBricks(List<Brick> bricks) {
		// deallocate old bricks
		bricks.clear();

		for (int iX = 0; iX < Parametros.COUNT_BLOCKS_X; ++iX) {
			for (int iY = 0; iY < Parametros.COUNT_BLOCKS_Y; ++iY) {
				bricks.add(new Brick((iX + 1) * (Parametros.BLOCK_WIDTH + 3) + 22,
						(iY + 2) * (Parametros.BLOCK_HEIGHT + 3) + 20));
			}
		}
	}

    //Método constructor de la clase Arkanoid
	public Arkanoid() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Establecer operación por defecto al cerrar
		this.setUndecorated(false);                         //Ocultar o no barra de de título
		this.setResizable(false);                           //Permitir o no cambiar el tamaño de la ventana
		this.setSize(Parametros.SCREEN_WIDTH, Parametros.SCREEN_HEIGHT); //Establecer anchura y altura de la ventana
		this.setVisible(true);                              //Establecer la ventana como visible
		this.addKeyListener(this);                          //Añade el listener
		this.setLocationRelativeTo(null);                   //Establece localización relativa

		this.createBufferStrategy(2);                       //Crea doble buffer de memoria para dibujar

		initializeBricks(bricks); //Se inicializan los ladrillos

	}

	//Esta función será la primera que se ejecute en el código, iniciará nuestro juego
	void run() {
		//Pintar el fondo de negro
		BufferStrategy bf = this.getBufferStrategy();
		Graphics g = bf.getDrawGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		//Se guarda el estado actual del juego: en ejecución
		running = true;
		// Llama a la funci�n de generar pantalla de t�tulo
		pantallaPrincipal();
		while (running) {

			long time1 = System.currentTimeMillis();
			//Si el juego se encuentra en proceso de ejecución entra a la función de actualización y tras la evaluación de los nuevos 
			//valores actualiza las imágenes en pantalla con drawScene. Utilizamos la función sleep para reducir la tasa de fotogramas.
			if (!scoreboard.gameOver && !scoreboard.win) {
				tryAgain = false;
				update();
				drawScene(ball, bricks, scoreboard);

				// Para reducir la tasa de fotogramas
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			//Si por el contrario se quiere reiniciar el juego se reinician las estadísticas y los objetos vuelven a la posición inicial, volviendo a la pantalla d inicio
			} else {
				if (tryAgain) {
					tryAgain = false;
					initializeBricks(bricks);
					scoreboard.lives = Parametros.PLAYER_LIVES;
					scoreboard.score = 0;
					scoreboard.win = false;
					scoreboard.gameOver = false;
					scoreboard.updateScoreboard();
					ball.x = Parametros.SCREEN_WIDTH / 2;
					ball.y = Parametros.SCREEN_HEIGHT / 2;
					paddle.x = Parametros.SCREEN_WIDTH / 2;
				}
			}
			//Calcula la tasa de fotogramas en base a el tiempo transcurrido entre la última actualización y este instante.
 			//Se muestra en el título de la ventana
			long time2 = System.currentTimeMillis();
			double elapsedTime = time2 - time1;

			lastFt = elapsedTime;

			double seconds = elapsedTime / 1000.0;
			if (seconds > 0.0) {
				double fps = 1.0 / seconds;
				this.setTitle("FPS: " + fps);
			}

		}
		//Cierra la ventana
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));

	}

	private void update() {
		//Lleva el recuento del tiempo transcurrido para actualizar a una cadencia constante
		currentSlice += lastFt;
		//Actualiza la pelota, la raqueta y comprueba las colisiones para destruir un ladrillo
		for (; currentSlice >= Parametros.FT_SLICE; currentSlice -= Parametros.FT_SLICE) {
			
			ball.update(scoreboard, paddle);
			paddle.update();
			testCollision(paddle, ball);

			Iterator<Brick> it = bricks.iterator();
			while (it.hasNext()) {
				Brick brick = it.next();
				testCollision(brick, ball, scoreboard);
				if (brick.destroyed) {
					it.remove();
				}
			}

		}
	}
	
	// Funci�n de generaci�n de la pantalla principal
	private void pantallaPrincipal(){
		// Mientras no se pulse enter...
		while(!tryAgain){
			// Indicamos el texto
			scoreboard.customText("\n\n Bienvenido a ARKANETSII,\n\n disfrute del juego!! \n\n Presiona enter para empezar");
			// Creamos el buffer de dibujo
			BufferStrategy bf = this.getBufferStrategy();
			Graphics g = null;
			//Dibuja el fondo negro y el texto
			try {
				g = bf.getDrawGraphics();
				g.setColor(Color.black);
				g.fillRect(0, 0, getWidth(), getHeight());
				// Indicamos que hemos ganado para internamente usar un tipo de fuente espec�fica
				scoreboard.win = true;
				scoreboard.draw(g);
				scoreboard.win = false;
				
			} finally {
				g.dispose();
			}
			
			// Lo mostramos
			bf.show();

			Toolkit.getDefaultToolkit().sync();
			// Esperamos para no bloquear al programa
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
	
	//Código para el dibujado de la escena
	private void drawScene(Ball ball, List<Brick> bricks, ScoreBoard scoreboard) {
		
		BufferStrategy bf = this.getBufferStrategy();
		Graphics g = null;
		//Dibuja el fondo negro, la pelota, la raqueta, los ladrillos y la puntuación.
		try {

			g = bf.getDrawGraphics();

			g.setColor(Color.black);
			g.fillRect(0, 0, getWidth(), getHeight());

			ball.draw(g);
			paddle.draw(g);
			for (Brick brick : bricks) {
				brick.draw(g);
			}
			scoreboard.draw(g);

		} finally {
			g.dispose();
		}

		bf.show();

		Toolkit.getDefaultToolkit().sync();

	}

	//Función para la comprobación de las teclas que son pulsadas
	@Override
	public void keyPressed(KeyEvent event) {
		//Si se pulsa escape para la ejecución poniendo la variable running a false.
		if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
			running = false;
		}
		//Si se pulsa enter se reinicia el juego activando la variable tryAgain
		if (event.getKeyCode() == KeyEvent.VK_ENTER) {
			tryAgain = true;
		}
		//Inicia el movimiento a izquierda o derecha según pulsemos las flechas izquierda o derecha
		switch (event.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			paddle.moveLeft();
			break;
		case KeyEvent.VK_RIGHT:
			paddle.moveRight();
			break;
		default:
			break;
		}
	}

	//Detiene el movimiento iniciado por la función keyPressed
	@Override
	public void keyReleased(KeyEvent event) {
		switch (event.getKeyCode()) {
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_RIGHT:
			paddle.stopMove();
			break;
		default:
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}
	//Función main que arranca la ejecución a través del método run
	public static void main(String[] args) {
		new Arkanoid().run();
	}

}
