package pgpi_arkanoid;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class ScoreBoard {
	
	// Variables propias de la clase
	int score = 0; // puntuaci�n
	int lives = Parametros.PLAYER_LIVES; // Vidas restantes del jugador
	boolean win = false; // La condici�n de si ha ganado 
	boolean gameOver = false; // la condici�n de si ha perdido
	String text = ""; // Texto a representar en el cuadro de juego

	Font font;
	
	// Inicializaci�n de la clase
	ScoreBoard() {
		font = new Font(Parametros.FONT, Font.PLAIN, 12);
		text = "Welcome to Arkanoid Java version";
	}

	// Funci�n que cada vez que se rompa un ladrillo, se encarga de incrementar la puntuaci�n y comprobar si se ha ganado, es decir, rotos todos
	void increaseScore() {
		score++;
		// Si se han roto todos los ladrillos, se monta el mensaje de victoria
		if (score == (Parametros.COUNT_BLOCKS_X * Parametros.COUNT_BLOCKS_Y)) {
			win = true;
			text = "You have won! \nYour score was: " + score
					+ "\n\nPress Enter to restart";
		} else { // En caso contrario, se actualiza la barra de puntuaci�n actual
			updateScoreboard();
		}
	}

	// Funci�n llamda en caso de perder la partida
	void die() {
		// Se pierde una vida
		lives--;
		// En caso de no quedar m�s vida se muestra el mensaje de game over
		if (lives == 0) {
			gameOver = true;
			text = "You have lost! \nYour score was: " + score
					+ "\n\nPress Enter to restart";
		} else { // En caso contrario, se actualiza la barra de `puntaciones con una vida menos
			updateScoreboard();
		}
	}
	
	// Funci�n que permite insertar un texto personalizado
	void customText(String text){
		this.text = text;
	}

	// Actualiza la barra de puntuaci�n y vida con los valores que presente este actualmente
	void updateScoreboard() {
		text = "Score: " + score + "  Lives: " + lives;
	}

	// Funci�n principal de la clase, se encarga de pintar sobre el lienzo g, el texto
	void draw(Graphics g) {
		// Elecci�n del estilo de texto seg�n si se sigue el juego o se ha perdido/ganado
		if (win || gameOver) { // En el segundo caso
			font = font.deriveFont(35f); // Fuente de tama�o 35
			FontMetrics fontMetrics = g.getFontMetrics(font);
			g.setColor(Color.WHITE); // Letras de color blanco
			g.setFont(font);
			int titleHeight = fontMetrics.getHeight();
			int lineNumber = 1;
			// Se escribe el mensaje, en la parte central de la pantalla, teniendo en cuenta los saltos de l�neas
			for (String line : text.split("\n")) {
				int titleLen = fontMetrics.stringWidth(line);
				g.drawString(line, (Parametros.SCREEN_WIDTH / 2) - (titleLen / 2),
						(Parametros.SCREEN_HEIGHT / 4) + (titleHeight * lineNumber));
				lineNumber++;

			}
			// En caso contrario, el juego continue...
		} else {
			font = font.deriveFont(34f); // Tama�o de fuente 34
			FontMetrics fontMetrics = g.getFontMetrics(font);
			g.setColor(Color.WHITE); // color blanco las letras
			g.setFont(font);
			int titleLen = fontMetrics.stringWidth(text);
			int titleHeight = fontMetrics.getHeight();
			// La cadena esta vez se coloca en la parte superior, para no interfereir con el juego
			g.drawString(text, (Parametros.SCREEN_WIDTH / 2) - (titleLen / 2),
					titleHeight + 15);

		}
	}

}
