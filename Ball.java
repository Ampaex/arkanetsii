package pgpi_arkanoid;

import java.awt.Color;
import java.awt.Graphics;

import pgpi_arkanoid.GameObject;

class Ball extends GameObject {

	double x, y; // Coordenaas x e y del centro
	double radius = Parametros.BALL_RADIUS; // Radio de la pelota
	// Velocidad en el eje x e y
	double velocityX = Parametros.BALL_VELOCITY; 
	double velocityY = Parametros.BALL_VELOCITY;

	public Ball (int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	// Funci�n encargada de dibujar la pelota sobre el lienzo
	void draw(Graphics g) {
		g.setColor(Color.RED);
		g.fillOval((int) left(), (int) top(), (int) radius * 2,
				(int) radius * 2);
	}
	
	// Funci�n encargada de actualizar la posici�n de la pelota
	void update(ScoreBoard scoreBoard, Paddle paddle) {
		// Se incrementa la velocidad en ambos ejes
		x += velocityX * Parametros.FT_STEP;
		y += velocityY * Parametros.FT_STEP;
		
		// Se suma/resto al centro, el radio actual, y se comprueba ... 
		if (left() < 0) // En caso de chocarse contra la pared izquierda, la velocidad pasa la derecha
			velocityX = Parametros.BALL_VELOCITY;
		else if (right() > Parametros.SCREEN_WIDTH) // Idem pero para la derecha
			velocityX = -Parametros.BALL_VELOCITY;
		if (top() < 0) { // Idem pero con el techo
			velocityY = Parametros.BALL_VELOCITY;
		} else if (bottom() > Parametros.SCREEN_HEIGHT) { // Idem pero con el suelo
			// En este caso se produce el game over, por tanto, se lo comunica a la barra de puntuaci�n
			velocityY = -Parametros.BALL_VELOCITY;
			x = paddle.x;
			y = paddle.y - 50;
			scoreBoard.die();
		}

	}

	/*
	 * Funciones heredadas de gameObject, se encargan de devoler la posici�n, en el eje x o y, del cuerpo para su posici�n actual + su borde, es este caso el radio
	 * para las 4 direcciones
	 */
	double left() {
		return x - radius;
	}

	double right() {
		return x + radius;
	}

	double top() {
		return y - radius;
	}

	double bottom() {
		return y + radius;
	}

}
