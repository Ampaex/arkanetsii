package pgpi_arkanoid;

import pgpi_arkanoid.GameObject;

// Clase que define un rectángulo en el juego:
class Rectangle extends GameObject {
	
	// Información geométrica del rectángulo:
	double x, y; // Punto central del rectángulo
	double sizeX; // Longitud del lado horizontal
	double sizeY; // Longitud del lado vertical

	// Posiciones X en las que se sitúan los dos lados verticales:
	double left() {
		return x - sizeX / 2.0;
	}

	double right() {
		return x + sizeX / 2.0;
	}

	
	// Posiciones Y en las que se sitúan los dos lados horizontales:
	double top() {
		return y - sizeY / 2.0;
	}

	double bottom() {
		return y + sizeY / 2.0;
	}

}
