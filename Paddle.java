package pgpi_arkanoid;

import java.awt.Color;
import java.awt.Graphics;

//Clase que define la paleta del juego
class Paddle extends Rectangle {
	
	//Como unica propiedad tiene su velocidad (por defecto a cero)
	double velocity = 0.0;

	//Constructor de la clase (recibe la posición de la paleta)
	public Paddle(double x, double y) {
		//Posición
		this.x = x;
		this.y = y;
		//Tamaño
		this.sizeX = Parametros.PADDLE_WIDTH;
		this.sizeY = Parametros.PADDLE_HEIGHT;
	}

	//Actualiza la posición de la paleta en la pantalla
	void update() {
		x += velocity * Parametros.FT_STEP;
	}

	//Deja de mover la paleta poniendo su velocidad a cero
	void stopMove() {
		velocity = 0.0;
	}

	//Mueve la paleta a la izquierda
	void moveLeft() {
		if (left() > 0.0) {
			velocity = -Parametros.PADDLE_VELOCITY;
		} else {
			velocity = 0.0;
		}
	}

	//Mueve la paleta a la derecha
	void moveRight() {
		if (right() < Parametros.SCREEN_WIDTH) {
			velocity = Parametros.PADDLE_VELOCITY;
		} else {
			velocity = 0.0;
		}
	}

	//Dibuja la paleta en pantalla
	void draw(Graphics g) {
		g.setColor(Color.RED); //Color en rojo
		g.fillRect((int) (left()), (int) (top()), (int) sizeX, (int) sizeY);
	}

}
