package pgpi_arkanoid;

import java.awt.Color;
import java.awt.Graphics;

//Clase que define los ladrillos (que la bola destruye)
public class Brick extends Rectangle {

	//Propiedad que indica si ha sido destruido o no (False por defecto)
	boolean destroyed = false;

	//Constructor de la clase (Recibe la posición del ladrillo)
	Brick(double x, double y) {
		//Posición del ladrillo
		this.x = x;
		this.y = y;
		//Tamaño del ladrillo
		this.sizeX = Parametros.BLOCK_WIDTH;
		this.sizeY = Parametros.BLOCK_HEIGHT;
	}

	//Función que dibuja el ladrillo
	void draw(Graphics g) {
		g.setColor(Color.YELLOW); //En color amarillo
		g.fillRect((int) left(), (int) top(), (int) sizeX, (int) sizeY);
	}
}
